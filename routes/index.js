var express = require('express');
var router = express.Router();
var fetch = require('node-fetch');
var wLocation = "";


var jsonWeather;
var jsonLocation;
var long, lat;

const getData =  async url => {
    try {
        var response = await fetch(url);
        jsonLocation = await response.json();
        if(jsonLocation == {}){
            console.log("nichts gefunden");
        }
        lat =  jsonLocation.data[0].latitude,function (err, lat) {
            if(err){
                console.log("An error has occurred. Abort everything!");
                return callback(err);
            }

        };
        long = jsonLocation.data[0].longitude;
        console.log("Lat: " + lat  + " ,Long: " + long);

        var weather = "https://api.openweathermap.org/data/2.5/onecall?lat="+ lat + "&lon=" + long + "&units=metric&exclude=hourly,minutely&appid=3aa3cc4046e615662e68a553b680837e";

        response = await fetch(weather);
        jsonWeather = await response.json();



    } catch (error) {
        console.log(error);
    }
};



router.get('/', function (req, res) {
    jsonLocation = {};
    jsonWeather = {};
    res.render('index');

});

router.post('/submit', function  (req, res) {
    var counter  = 0;
    wLocation = req.body.weatherLocation;
    console.log('Location: ' + wLocation);
    var location = "http://api.positionstack.com/v1/forward?access_key=a1d9b7d519a7edfa4e2f705096a42764&query="+wLocation;

    // getData(location, function (err, res) {
    //     if(err)
    //         return console.error(err)
    //
    //     res.render('forecast', {weather : test});
    //
    // });
    getData(location);
    setTimeout(function () {
        res.render('forecast', {weather : jsonWeather, wLocation});
    }, 3000);

});


module.exports = router;