GetYourDailyWeather:

Mit GetYourDailyWeather können Sie sich die Wetterdaten über 7 Tage, für eine bestimmte Region anzeigen lassen.
Hierfür wird erst eine Location angegeben und zwar wie folgt: "Stadt, Land". Also zum Beispiel, "Rom, Italien" oder "Gießen, Deutschland".
Dann wird anhand einer REST API aus dieser Location die nötigen Daten erstellt und das sind die Latitude und die Longitude. Danach wird eine 
zweite REST-API, mit den zuvor zurückgebenen Daten (Latitude, Longitude), nach dem Wetter in dieser Region abgefragt. Danach wird eine Übersicht über die
nächsten 7 Tage erstellt.

Benutze REST-API sind:
    1. positionstack (Location -> Latitude, Longitude)
    2. openweathermap (Latiude, Longitude -> Wetterdaten)
    
