var express = require('express');
var app = express();

// Routes
var index = require('../routes/index');
var forecast = require('../routes/forecast');

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended: true}))




app.set('port', process.env.PORT  || 3000);


// View Engine
app.set('views', './views');
app.set('view engine', 'jade');

// Erste Middleware
app.use(express.static('./public'));

//Routen
app.use('/', index);
app.use('/forecast', forecast);


// Error Handling
app.use(function (req, res) {
    res.type('text/plain');
    res.status(404);
    res.send('404 - Not Found');
});

app.use(function (err, req,res, next) {
    console.error(err.stack);
    res.type('text/plain');
    res.status(500);
    res.send('500 - Internal Error, Location Not Found. Please try again!');
});

exports.start = function () {
    app.listen(app.get('port'), function (){
        console.log('Express ready on http://127.0.0.1:' + app.get('port'));

    });
};